---
home: true
heroImage: /PBST-Logo.png
actionText: Read the handbook →
actionLink: pbst/
---
::: warning NOTE:
As of 
 - ``EU Date Format: 9-3-2020 ``
 - ``US Date Format: 3-9-2020``

This version of the handbook is now an official PBST Handbook. As stated by Csdi on that specific date:
![img](https://i.imgur.com/ZVyPdZb.png)

The official handbook can be found [here](https://devforum.roblox.com/t/pinewood-builders-security-team-handbook-11-10-2019/385368). 
Please support the official version as well :smile:
:::

::: danger WARNING!
Please read through all of the rules very carefully. Any changes to the rules will be made clear. The rules have been categorized into who they apply to, however some rules may apply to more than one group of PBST members, in which case it will be made clear. Punishments for breaking a rule depend on what rule and the severity. Punishments can range from a simple warning, demotion, or even blacklist from PBST. 
:::

<center>
<a href="https://www.netlify.com">
  <img src="https://www.netlify.com/img/global/badges/netlify-color-accent.svg"/>
</a></center>
