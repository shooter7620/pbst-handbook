---
home: true
heroImage: /PBST-Logo.png
actionText: Vamos a ello →
actionLink: pbst/
---
::: warning NOTA: 
A partir del
 - `Formato de Fecha de UE: 9-3-2020`
 - `Formate de Fecha de EEUU: 3-9-2020`

Esta versión del manual es ahora un manual oficial del PBST. Según lo indicado por Csdi en esa fecha específica: ![img](https://i.imgur.com/ZVyPdZb.png)

El manual oficial se puede encontrar [aquí](https://devforum.roblox.com/t/pinewood-builders-security-team-handbook-11-10-2019/385368). Por favor apoya la versión oficial también :smile: 
:::

::: danger ¡ADVERTENCIA! 
Por favor, lee con atención todas las normas. Se aclararán los cambios de las normas. Las reglas han sido categorizadas en a quién se aplican, sin embargo, algunas reglas pueden aplicarse a más de un grupo de miembros del PBST, en cuyo caso se aclarará. Las sanciones por incumplir una regla dependen de qué regla y de la severidad. Las sanciones pueden variar desde una simple advertencia, degradación de rango, o incluso ser incluido en lista negra del PBST. 
:::

<center>
<a href="https://www.netlify.com">
  <img src="https://www.netlify.com/img/global/badges/netlify-color-accent.svg"/>
</a></center>