# Useful Tips For Combat

## Basic Marksmanship
::: Tip Hitscan Information 
All of the ranged weapons accessible to their respective ranks are internally hitscan, but with a cosmetic tracer. This doesn't mean that you shouldn't give lead to whichever target you are shooting at with guns, because lag can affect the weaponry. The only exceptions are the loadouts at [PBSTTF](https://www.roblox.com/games/298521066/PBST-Training-Facility), [PBRF](https://www.roblox.com/games/7692456/Pinewood-Research-Facility?refPageId=8e19f3f4-c84c-4f9f-a58f-9e4bb1378487), and [PBHQ](https://www.roblox.com/games/7956592/Pinewood-Builders-HQ?refPageId=2a31733e-50d4-4584-bb46-c6ec0247f1a1), with them being hitscan/having extremely fast projectiles.
:::

### Guns and the auxillary ranged weapon, "Taser"

Tiers get access to a Pistol, Rifle, and/or Submachine Gun depending on their rank. They also recieve a Taser and Riot Shield(More on that later).

First, we need to know as mentioned above, that these guns are internally hitscan. This means you must learn how to lead your target, or predict where they will be when you shoot and the projectile reaches the target, because you need to account for lag. This obviously requires a lot of practice to get a hang of, and that's why there are resources to help with that.

**At the [PBSTAC](https://www.roblox.com/games/1564828419/PBST-Activity-Center?refPageId=6316b791-ed4d-4dfa-962b-eb620cf7e16c), there are three ways to improve your accuracy as a Tier:**

* Patrol the Activity Center and practice your aim by shooting down threats(NPCs, or Raiders and Mutants).
* Practice the Gun Bot Self-Training course, which can be found in Sector 2.
* Go to Sector 3 and practice your aim at the shooting range, preferrably with a **Tier 3+**, as they have access to configure the targets for you at that activity.

::: tip Tip 
A helpful tip is to always aim at the center of your target, then lead it, depending on the range. 
:::

#### Taser

The Taser is a ranged weapon with the purpose and function of stunning your enemy for a short time. This tool is very useful for gaining an edge over your opponent, as they are unable to move, and thus makes shooting them much easier. The stun effect usually lasts around 7-10 seconds, more than enough time to finish someone off while they can't move. Example of stunned enemy: 

![img](/publicimage.png)

::: danger Beware!
While the Taser can stun people, it is effectively negated against people who know how to use the Personal Rocketship and Jetpack gears at [PBCC](https://www.roblox.com/games/17541193/Pinewood-Computer-Core?refPageId=72e36cef-d007-45f6-9dd2-bf3f260db1a7) to their advantage. 

It is recommended not to be reliant on the Taser while patrolling PBCC. **You have been warned.** 
:::

### Shooting Behind Cover

Shooting behind cover is a tactic that allows for damamging your opponents without taking damage back. An example of this is hiding in the reactor power room in PBCC or PBSTAC, behind one of the corners.

![image](/CornerShot.png)

There are other places to use cover effectively against enemies, such as the barrier right outside the cooling fans control room, to the right of its enterance.

## OP Weapons at PBCC

The [OP Weapons](https://www.roblox.com/game-pass/776368/OP-Weapons) gamepass is a loadout that consists of the following items:
* Gamma Spec Sword
* OP Pistol
* OP Rifle
* OP Submachine Gun

The Gamma Spec Sword is a very powerful melee weapon, killing in just a second of a lunge attack(This will be a weapon we come back to later).

The OP Pistol and OP Rifle deal about the same amount of damage as PBST/TMS Pistols and Rifles. The main difference when it comes the OP Weapons is the very powerful melee weapon, and a very powerful Submachine Gun to go along with the melee. The Submachine Gun deals 25 damage per hit on the limbs or torso. A headshot deals around 30+ damage. 

::: warning REMINDER:
If you are using OP Weapons on-duty as PBST, you are to treat them as if they are PBST loadouts. This means you must still follow the weapon usage rules in the PBST Handbook. 
:::


## Melee Combat
### Sword Fighting Basics

First, we need to know that swords are only accessible on patrol to Tier 4+, or those with OP Weapons. If you are without a sword, there will be later tips included in here about how to effectively use a baton to finish off an opponent.

There are many ways to practice Sword Fighting, such as:
* Playing other games with Sword Fighting combat elements.
* Practicing Swordfighting with others in the Sector 1 Gladiator or Sword Fighting Arena, or the Sector 2 SF Bots with the help of a Tier 4+.
* Having someone experienced fight you and give tips on what to improve.

A very important tip is to time your sword lunges correctly. If you try to attack an opponent while their sword is lunging, it is a guaranteed way to end up with your Roblox character in pieces. For additional help, watch the [Lunge Timing Video](https://www.youtube.com/watch?v=hpTEB0c0esI) by Roblox Sword Fighting tips to get an additional understanding of this.

Another tip is to keep to the left of your enemy, to avoid their sword when attempting to strike. Here is a visual example (Credit to RazaMK):

![image](/iaw.png)

#### Additional Helpful References

* [Camera Angles Sword Fighting Tutorial](https://www.youtube.com/watch?v=axWQpwioo3g)
* [Maximizing Lunge Damage](https://www.youtube.com/watch?v=BDyj5zkSkrs)

#### Using The Baton Effectively

The Baton is a bit weaker than any sword that you might be able to use while patrolling, so it is important that you learn how to effectively use it. The Baton isn't something you charge in with against an OP Weapons user, it is a better solution to instead damage or stun the enemy first, and then use the baton.

### Riot Shield

The Riot Shield Tiers are able to use blocks projectiles from guns. The downside to this is that it doesn't cover your Roblox character completely, and that you cannot attack back, along with only blocking projectiles your character is facing towards.

::: warning Watch Out!
While the Riot Shield can block ranged attacks, it is not able to block explosions from a rocket launcher or protect you from melee attacks.
:::

## Final Things

**Always remember that practice allows you to get better at combat**, and improve your skills. 
Practicing of combat skills does not have to be limited to games by Pinewood Builders, there are plenty of other games where you can fight others and learn from those places. 
You can also ask others experienced in combat to give you tips or help you improve your skill.


# References
* [Tools and Loadouts](https://pinewood.fandom.com/wiki/Tools_and_Loadouts)
* [Camera Angles Sword Fighting Tutorial](https://www.youtube.com/watch?v=axWQpwioo3g)
* [Maximizing Lunge Damage](https://www.youtube.com/watch?v=BDyj5zkSkrs)
* [Lunge Timing Video](https://www.youtube.com/watch?v=hpTEB0c0esI)
* Sword Fighting [Image](https://cdn.discordapp.com/attachments/603138786687189023/698806674466537492/iaw.png)  provided by RazaMK
* [OP Weapons](https://www.roblox.com/game-pass/776368/OP-Weapons)
* [PBCC](https://www.roblox.com/games/17541193/Pinewood-Computer-Core?refPageId=72e36cef-d007-45f6-9dd2-bf3f260db1a7)
* [PBSTAC](https://www.roblox.com/games/1564828419/PBST-Activity-Center?refPageId=6316b791-ed4d-4dfa-962b-eb620cf7e16c)
* [PBSTTF](https://www.roblox.com/games/298521066/PBST-Training-Facility)
* [PBRF](https://www.roblox.com/games/7692456/Pinewood-Research-Facility?refPageId=8e19f3f4-c84c-4f9f-a58f-9e4bb1378487)
* [PBHQ](https://www.roblox.com/games/7956592/Pinewood-Builders-HQ?refPageId=2a31733e-50d4-4584-bb46-c6ec0247f1a1)
* All other images were sourced from shooter7620's screenshots.
