
# Welcome to the handbook!
This is the index page. Here you will be able to see any page that's in the handbook. Click on one of the links to view the page.

* [PBST Handbook](handbook/)
  * [Going on duty to patrol](handbook/#going-on-duty-to-patrol)
    * [Uniform](handbook/#uniform)
    * [Ranktag](handbook/#ranktag)
    * [Going off duty](handbook/#going-off-duty)
    * [Usage of weapons](handbook/#usage-of-weapons)
    * [How to deal with rulebreakers](handbook/#how-to-deal-with-rulebreakers)
    * [Kill on Sight (KoS)](handbook/#kill-on-sight-kos)
  * [Important facilities to patrol](handbook/#important-facilities-to-patrol)
    * [Pinewood Computer Core](handbook/#pinewood-computer-core)
    * [PBST Activity Center (PBSTAC)](handbook/#pbst-activity-center-pbstac)
  * [Other groups you may encounter](handbook/#other-groups-you-may-encounter)
    * [Pinewood Emergency Team](handbook/#pinewood-emergency-team)
    * [The Mayhem Syndicate](handbook/#the-mayhem-syndicate)
  * [Trainings and ranking up](handbook/#trainings-and-ranking-up)
    * [Training](handbook/#training)
  * [Ranks](handbook/#ranks)
    * [Tiers](handbook/#tiers)
    * [Tier 4 (aka, Special Defence, SD)](handbook/#tier-4-aka-special-defence-sd)
    * [Trainers](handbook/#trainers)
* [Points and ranking up](ranks-and-ranking-up/)
  * [Abbreviations](ranks-and-ranking-up/#abbreviations)
  * [How do you earn points](ranks-and-ranking-up/#how-do-you-earn-points)
  * [Ranking up](ranks-and-ranking-up/#ranking-up)
  * [Tier Evaluations](ranks-and-ranking-up/#tier-evaluations)
  * [When will points be logged?](ranks-and-ranking-up/#when-will-points-be-logged)
  * [Self Trainings](ranks-and-ranking-up/#self-trainings)
  * [Practice Trainings](ranks-and-ranking-up/#practice-trainings)
* [Tier 4 and Trainer commands](training-hosters/)
  * [Possible arguments](training-hosters/#possible-arguments)
  * [What commands are generally used in trainings?](training-hosters/#what-commands-are-generally-used-in-trainings)
  * [Suggestions for aliases](training-hosters/#suggestions-for-aliases)
