# Points and ranking up

## How do you earn points
Points can be earned in a few ways, you can follow a ST (Self Training). Be in a training, or be in events like raids, and other things. 
Depending on what you do you get points based on what you're doing. Being in a raid (On the side of PBST (Pinewood Builders Security Team)).

Trainings are also an option, trainings get announced on our group wall. Trainings can be hosted by:
 * Trainers
 * Tier 4

Anyone is able to assist in a training, but the Host/Trainer chooses who is going to be an assistant, asking for assistant doesn't help in this case. This will only lower your chances of becoming an assistant, more information about some of the rules can be found [here](../handbook/#training).

As the last option, you can always follow an ST. These trainings are made specificly for people to earn points in a different way, and to practice your skills in a specific activity. More informations will be given [here](#self-trainings)


## TMS Raids
TMS raids are the most recommended way to earn points. If they start raiding a server to cause a melt- or freezedown, you can earn points quite easily by fighting against them. Make sure to keep an eye out for when they start joining your game.

You’ll earn 5 points if you’re with PBST for the entire side of the raid, and even more if you’re showing outstanding performance or leadership. However, if you only attend a part of the raid or perform poorly, you’ll get fewer points.

A Tier 4 or Trainer will supervise the raid and monitor everyone’s performance to give points accordingly. They may do periodic checks to see who is attending on PBST’s side, so make sure you follow whatever instructions they give you. **If no Tier 4 or Trainer is available, an Operative or higher from TMS will do the PBST attendance.**

It is recommended you have Tier weapons or other good weapons, if the server is almost full and you’re trying to attend the raid with no weapons other than the baton, you may be removed from the game.

If a raid starts in your server and you wish not to attend, you will be asked to go off-duty (read the corresponding chapter above).

# Trainings
Tier 4’s and Trainers regularly host trainings, which are an alternative way to earn points. The schedule of these trainings can be found at the [Pinewood Builders Data Storage Facility](https://www.roblox.com/games/1428153850/Pinewood-Builders-Data-Storage-Facility). Keep an eye on the group shout as well.

The better you perform, the more points you get. The maximum amount of points you can earn in a normal training is 5. Every Saturday at 5:00 PM UTC there is a Mega Training, where double points can be earned.. Keep an eye on the group shout as well.



## Ranking up
Ranking up requires points, which can be earned in training, ST's and patrols. If you get the required amount of points for the next rank, you’ll be promoted. The required amount of points for any rank can be found at the spawn at [PBST Activity Center](https://www.roblox.com/games/1564828419/PBST-Activity-Center), ~~though you can ignore the part of evaluations, those are only in place to become Tier 4~~. This has now changed. Please read the message below...

:::danger Keep in mind!
When you're a ``Tier 1+`` you may be punished harder for mistakes you make, you're supposed to be a role model for all the cadets and visitors in **Pinewood Facilities**. 
This has no exeption (Unless stated otherwise as the T4 uniform rule)  
:::


## Tier Evaluations
Once you reach 100 points, you must participate in a Tier evaluation if you want to get the **Tier 1** rank. 
It is not recommended to get more points before your evaluation, if you take the eval while having more than *150 points **you’ll be set back** to 150*.

As with normal trainings, the scheduled Tier evaluations can be found on the schedule at [Pinewood Builders Data Storage Facility](https://www.roblox.com/games/1428153850/Pinewood-Builders-Data-Storage-Facility). 
They will be hosted regularly to accommodate all timezones.

There is a specialized server for Tier evaluations, use the command ``!pbstevalserver`` to get there. This only works if you have 100+ points.

A Tier evaluation consists of 2 parts: a quiz and a test on patrolling skills. The training rules will be heavily enforced in this eval, not following them will result in an immediate fail. If you pass one part but fail the other, you may try again at another eval and skip the part you passed before. (There is a database where we log all the people who failed and passed the quiz or the patrol)

During the quiz, you will receive 8 questions about various topics including this handbook and training rules. The questions will vary in difficulty, some are easy but others require more thinking. You need to score at least 5/8 to pass. Answering must be done privately through whisper chat or a Private Message system sent by the host.

During the patrol test, you will receive Tier 1 loadouts and be tested on skills like your abilities in combat, teamwork, and following the handbook correctly. A number of **Tier 3**'s will work against you in this part, as they will try to bring the core to melt- or freezedown.

## When will points be logged?
This depends on what trainer is availible and has the time to log points. And we can not say this enough, but **do not ask trainers for points to be logged**. This will only be annoying for the trainers logging the points in question. And it's generally spamming if alot of people say **SIR SIR POINTS PLEASE LOG POINTS**. Trainers are able to log points, this means they can also **SUBTRACT** them. As a warning I can give you, **DON'T** ask for points to be logged, unless a trainer tells you to ask them to be logged (Take image as example)

<img class="no-medium-zoom zooming" src="https://i.imgur.com/yNHkmzm.png" alt="demo" width="1228"/>

## Self Trainings
When you're self training, you able to earn points without being in a training, the points can change depending on your rank. If you progress in Tiers, you also earn less points in self training. This is due to the fact that you should be able to do a higher level if you're a higher Tier.

An image created by **vgoodedward** shows this is a good way:
<img class="no-medium-zoom zooming" src="https://i.imgur.com/ho3u0a5.png" alt="demo" width="1228"/>

## Practice Trainings

Practice Training also known as PT's can be hosted by any PBST member at any facility (preferably PBSTAC or PBSTTF). 
These Trainings are unofficial and you won’t earn Points in any way. 

Normal PT's will usually consist of a few attendees with a Tier or Cadet standing on the pad. 
Before a PT can start the person hosting the PT must state that it’s not an official  Training and explain that you won’t earn any points. 
You are not allowed to host a PT 30 minutes before an official Training (Or 15 minutes with permission from the host).

This means a PT must end 30-15m before an actual training starts

If a person claims to be a real Trainer or Tier 4, take a screenshot and report the user to an HR. 


# Ranks
Cadet is the default rank that PBST get when they join the group.

## Tiers
Once you have passed the Tier evaluation, you will be promoted to **Tier 1**. With this rank, you will get a new loadout in Pinewood facilities: a more powerful baton, a riot shield, a taser, and a PBST pistol. These weapons can be found in the Security room at Pinewood facilities. Cadets are not allowed in the loadout rooms of Tiers.

In order to get Tier 2 or Tier 3, you only need to get the required amount of points. With Tier 2 a rifle is added to your PBST loadout, and At Tier 3 you receive a submachine gun along with the rest.

Tiers are expected to be a role model for Cadets. Any violations of the rules may result in a larger punishment. Especially Tier 3’s are expected to be the perfect representation of PBST at its best. You have been warned.

Tiers have access to the ``!call PBST`` command, which they can use for an imminent melt- or freezedown or a raider. This is to be used wisely.

As a Tier, you may be selected to assist at a training or an eval, where you will be given temporary admin powers which are strictly for that specific training. **Abuse of these powers will result in severe punishment.**

## SD's
Tier 3’s who reach 800 points are eligible for an SD evaluation to become Tier 4. If chosen, you are tasked to host a PBST Training and your performance will be closely watched. If you pass, you are given the title “``Passed SD Eval``” until the Trainers choose to promote you to Tier 4. Having Discord is required.

As Tier 4, you receive the ability to place a KoS in Pinewood facilities, you can order room restrictions, and you don’t have to wear a uniform anymore. You will also receive Kronos mod at Pinewood’s training facilities, this is to be used **responsibly**.

Tier 4’s can host trainings with permission from a Trainer. They may host 6 times per week (Mega’s not included), with a maximum of 2 per day. Tier 4’s may not request a training more than 3 days ahead of time. There also has to be a 2-hour gap at least between the end of one training and the start of another.

## Trainers
To become a Trainer, all the current Trainers have to vote on your promotion. Only Tier 4’s are eligible for this promotion.

Trainers can host trainings without restrictions, though the 2-hour gap rule still applies. Trainers are also responsible for some of PBST’s more administrative tasks, like points logging and promotions

:::danger Do not ask for promotions
We can't say it enough, do not ask for points, promotions or anything related to this. You earn points like everyone does, and these points define what rank you are in the group. Trainers and SD's have the authority to deduct your points if they deem it necessary, so please. Wait for points to be logged, and earn them like everyone else.
:::
