module.exports = [{
    text: 'Hoofdpagina',
    link: '/nl/'
},
{
    text: 'PBST Handboek',
    link: '/nl/pbst/'
},
{
    text: 'Pinewood',
    items: [{
            text: 'PET-Handbook',
            link: 'https://pet.pinewood-builders.com'
        },
        {
            text: 'TMS-Handbook',
            link: 'https://tms.pinewood-builders.com'
        }
    ]
}
]